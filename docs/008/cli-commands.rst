**********************
Command Line Interface
**********************

This document is a prettier output of the documentation produced by
the command line client's ``man`` command. You can obtain similar pages
using the following shell commands.

::

   tezos-client -protocol ProtoALphaALph man -verbosity 3
   tezos-admin-client man -verbosity 3


.. _client_manual_008:

Client manual
=============

.. raw:: html
         :file: tezos-client-alpha.html


.. _admin_client_manual_008:

Admin-client manual
===================

.. raw:: html
         :file: ../api/tezos-admin-client.html


.. _signer_manual_008:

Signer manual
=============

.. raw:: html
         :file: ../api/tezos-signer.html


.. _baker_manual_008:

Baker manual
============

.. raw:: html
         :file: tezos-baker-alpha.html


.. _endorser_manual_008:

Endorser manual
===============

.. raw:: html
         :file: tezos-endorser-alpha.html


.. _accuser_manual_008:

Accuser manual
==============

.. raw:: html
         :file: tezos-accuser-alpha.html

.. _benchmark_tool_manual:

Benchmark tool manual
=====================

.. raw:: html
         :file: tezos-snoop.html
